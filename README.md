# topmeteosharedaccount
Fichier de configuration de [sharedaccount](https://framagit.org/snip/sharedaccount) pour le service TopMeteo (https://europe.topmeteo.eu/fr/).

## Configuration
Le fichier de configuration doit être nommé `sharedaccountconfig.py` :
```
cp sharedaccountconfig.topmeteo.py sharedaccountconfig.py
```

Il convient de définir le paramètre `reverse_domain` avec le nom de domaine du reverse-proxy :
```
reverse_domain = "topmeteo.example.net"
```

De plus, pour permettre la connexion automatique, il convient de définir les paramètres suivants :
```
        "username": "myuser",
        "password": "mypass",
```
