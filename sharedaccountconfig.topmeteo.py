#!/usr/bin/env python3

reverse_domain = "topmeteo.example.net"
real_domain = "europe.topmeteo.eu"
headers_to_add = {
    "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0",
    "DNT": "1",
    "Upgrade-Insecure-Requests": "1",
}
headers_to_delete = ["X-Forwarded-Proto", "X-Forwarded-For"]
cookie_name = "sessionid"
autologin = {
    "form_path_regex": "^(/[-a-zA-Z]+){2}/login/$",
    "submit_url": "https://europe.topmeteo.eu/fr/fr/login/?next=",
    "redirect_path": "/fr/fr/start/",
    "headers_to_add": {
        "Referer": "https://europe.topmeteo.eu/fr/fr/login/",
    },
    "params": {
        "username": "myuser",
        "password": "mypass",
    },
}

